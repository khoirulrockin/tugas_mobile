import 'dart:convert';

import 'package:http/http.dart' show Client, Response;
import 'package:minggu10_moviedb/model/popular_movies.dart';

class ApiProvider {
  String apiKey = 'a13830ff6ed50b1f20715ae6e899cf7c';
  String baseUrl = 'https://api.themoviedb.org/3';

  Client client = Client();

  Future<PopularMovies> getPopularMovies() async {
    // String url = '$baseUrl/movie/popular?api_key=$apiKey';
    // print(url);
    Response response =
        await client.get('$baseUrl/movie/popular?api_key=$apiKey');

    if (response.statusCode == 200) {
      return PopularMovies.fromJson(jsonDecode(response.body));
    } else {
      throw Exception(response.statusCode);
    }
  }
}
