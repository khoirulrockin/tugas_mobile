import 'dart:io';

void main(List<String> args) {
  var sentence = "I am going to be Flutter Developer";
  var exampleFirstWord = sentence[0];
  var exampleSecondWord = sentence.substring(2, 3);
  var thirdWord = sentence.substring(5, 10);
  var fourthWord = sentence.substring(11, 13); // lakukan sendiri
  var fifthWord = sentence.substring(14, 16); // lakukan sendiri
  var sixthWord = sentence.substring(16, 24); // lakukan sendiri
  var seventhWord = sentence.substring(25, 34); // lakukan sendiri
  print('First Word: ' + exampleFirstWord);
  print('Second Word: ' + exampleSecondWord);
  print('Third Word: ' + thirdWord);
  print('Fourth Word: ' + fourthWord);
  print('Fifth Word: ' + fifthWord);
  print('Sixth Word: ' + sixthWord);
  print('Seventh Word: ' + seventhWord);
}
