// importing dart:io file
import 'dart:io';

// import 'dart:svg';

void main() {
  print("Masukkan Nama Anda?");
  // Reading name
  var name = stdin.readLineSync();

  if (name == "") {
    print("Nama harus diisi");
  } else {
    print(
        "Pilih Peran Anda, ketikkan angka? \n1. Penyihir \n2. Guard \n3. Werewolf");
    var peran = stdin.readLineSync();

    if (peran == "") {
      print("Hallo, $name Pilih peranmu untuk memulai game!");
    } else if (peran == "1") {
      print(
          "Selamat datang di Dunia Werewolf, $name! \nHalo Penyihir $name, kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (peran == "2") {
      print(
          "Selamat datang di Dunia Werewolf, $name! \nHalo Guard $name, kamu akan membantu melindungi temanmu dari serangan werewolf");
    } else if (peran == "3") {
      print("Selamat datang di Dunia Werewolf, $name"
          "\nHalo Werewolf $name, Kamu akan memakan mangsa setiap malam!");
    } else {
      print("Tidak ada yang dipilih");
    }
  }
}
