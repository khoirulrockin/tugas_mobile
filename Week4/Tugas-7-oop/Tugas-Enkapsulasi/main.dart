import 'dart:io';
import 'lingkaran.dart';

void main(List<String> args) {
  Lingkaran bundar;
  double luasLingkaran;

  bundar = new Lingkaran();
  bundar.jariJari = -5;

  luasLingkaran = bundar.hitungLuas();
  print("$luasLingkaran cm2");
}
