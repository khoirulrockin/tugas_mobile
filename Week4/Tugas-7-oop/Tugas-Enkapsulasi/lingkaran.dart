class Lingkaran {
  late double _jariJari;

  void set jariJari(double value) {
    if (value < 0) {
      value *= -1;
    }
    _jariJari = value;
  }

  double get jariJari {
    return _jariJari;
  }

  double hitungLuas() {
    return this._jariJari * _jariJari * 3.14;
  }
}
