import 'dart:io';
import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main(List<String> args) {
  armor_titan art = armor_titan();
  attack_titan akt = attack_titan();
  beast_titan bt = beast_titan();
  human h = human();

  art.powerPoint = 2;
  akt.powerPoint = 4;
  bt.powerPoint = 6;
  h.powerPoint = 8;

  print("Power point Armor Titan : ${art.powerPoint}");
  print("Power point Attack Titan : ${akt.powerPoint}");
  print("Power point Beast Titan : ${bt.powerPoint}");
  print("Power point Human : ${h.powerPoint}");

  print("Sifat dari Armor Titan adalah " + art.terjang());
  print("Sifat dari Attack Titan adalah " + akt.punch());
  print("Sifat dari Beast Titan adalah " + bt.lempar());
  print("Sifat dari Human adalah " + h.killAlltitan());
}
