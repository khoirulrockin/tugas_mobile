import 'bangun_datar.dart';
import 'segitiga.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'dart:math';

void main(List<String> args) {
  Bangun_Datar bangun_datar = new Bangun_Datar();
  Persegi persegi = new Persegi(5);
  Lingkaran lingkaran = new Lingkaran(7);
  Segitiga segitiga = new Segitiga(3, 4);

  bangun_datar.keliling();
  print("Keliling Persegi dengan sisi ${persegi.sisi} adalah ${persegi.keliling()} cm");
  print("Keliling Lingkaran dengan jari-jari ${lingkaran.jari_jari} adalah ${lingkaran.keliling()} cm");
  print("Keliling Segitiga dengan alas ${segitiga.alas} & tinggi ${segitiga.tinggi} adalah ${segitiga.keliling()} cm");

  bangun_datar.luas();
  print("Luas Persegi dengan sisi ${persegi.sisi} adalah ${persegi.luas()} cm2");
  print("Luas Lingkaran dengan jari-jari ${lingkaran.jari_jari} adalah ${lingkaran.luas()} cm2");
  print("Luas Segitiga dengan alas ${segitiga.alas} & tinggi ${segitiga.tinggi} adalah ${segitiga.luas()} cm2");
}
