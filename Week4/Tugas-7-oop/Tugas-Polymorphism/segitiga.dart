import 'bangun_datar.dart';
import 'dart:math';

class Segitiga extends Bangun_Datar {
  late double alas;
  late double tinggi;
  late double miring;

  Segitiga(double alas, double tinggi) {
    this.alas = alas;
    this.tinggi = tinggi;
  }

  double phytagoras() {
    double hasil = alas * alas + tinggi * tinggi;
    miring = sqrt(hasil);
    return miring;
  }

  double keliling() {
    return alas + tinggi + phytagoras();
  }

  double luas() {
    return 0.5 * alas * tinggi;
  }
}
