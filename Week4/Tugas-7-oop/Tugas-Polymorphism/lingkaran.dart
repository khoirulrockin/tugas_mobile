import 'bangun_datar.dart';

class Lingkaran extends Bangun_Datar {
  late double jari_jari;

  Lingkaran(double jari_jari) {
    this.jari_jari = jari_jari;
  }

  double keliling() {
    return 2 * 3.14 * jari_jari;
  }

  double luas() {
    return 3.14 * jari_jari * jari_jari;
  }
}
