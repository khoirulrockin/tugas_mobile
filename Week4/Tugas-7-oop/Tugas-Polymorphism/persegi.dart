import 'bangun_datar.dart';

class Persegi extends Bangun_Datar {
  late double sisi;

  Persegi(double sisi) {
    this.sisi = sisi;
  }

  double keliling() {
    return 4 * sisi;
  }

  double luas() {
    return sisi * sisi;
  }
}
