import 'dart:async';

void main(List<String> args) async {
  var h = Human(); //inisialisasi t sebagai object dari class Human

  print("Luffy");
  print("Zoro");
  print("Killer");
  print(h.name);
  await h.getData();
  print("name 3: ${h.name}");
}

class Human {
  String name = "Nama character one piece";
  Future<void> getData() async {
    await Future.delayed(Duration(seconds: 5));
    name = "Hilmy";
    print("get data [done]");
  }
}
