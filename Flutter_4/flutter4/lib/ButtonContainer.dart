import 'package:flutter/material.dart';

void main() => runApp(ButtonContainer());

//membuat class BelajarImage yang secara default extends StatelessWidget
class ButtonContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false, //menghilangkan banner debug

        home: Scaffold(
            appBar: AppBar(
              title: Text("Button Widget with Container"), //Text di AppBar
              backgroundColor:
                  Color.fromARGB(255, 9, 49, 82), //backgroundColor di appBar
              actions: <Widget>[
                Icon(Icons.search),
              ],
            ),
            backgroundColor:
                Colors.blue, //memberi backgroundColor pada scaffold
            body: Column(
              children: [
                SizedBox(height: 60),
                Center(
                    child: Text(
                  'Belajar Button Widget',
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                )),
                SizedBox(height: 5),
                Center(
                    child: Text(
                  'Noga Muktiwati (E41200415) A',
                  style: TextStyle(fontSize: 12, color: Colors.white),
                )),
                SizedBox(height: 10),
                Image.asset('assets/images/noga.jpg'),
                SizedBox(height: 20),
                Container(
                    width: 180,
                    height: 55,
                    decoration: BoxDecoration(
                      color: Color.fromARGB(255, 241, 246, 247),
                      borderRadius: BorderRadius.circular(35),
                    ),
                    child: Row(
                      children: [
                        SizedBox(width: 20),
                        Text(
                          'Allah',
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.blue,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ))
              ],
            )));
  }
}
