import 'package:flutter/material.dart';
void main() => runApp(FormInput());
//membuat class BelajarImage yang secara default extends StatelessWidget
class FormInput extends StatelessWidget {
 @override
 Widget build(BuildContext context) {
 return MaterialApp(
    debugShowCheckedModeBanner: false,//menghilangkan banner debug
   
 home: 
 Scaffold(
  appBar: AppBar(
    title: Text("Form Login  Widget"), //Text di AppBar
    backgroundColor: Color.fromARGB(255, 9, 49, 82),//backgroundColor di appBar
    actions: <Widget> [
    Icon(Icons.search),
  ],
 ),
 
  body: Padding(
 padding: const EdgeInsets.all(8.0),
 child: Form(
 child: Column(
 children: <Widget>[
 TextFormField(
 decoration: InputDecoration(hintText: "Username"),
 ),
 TextFormField(
 obscureText: true,
decoration: InputDecoration(hintText: "Password"),
 ),
 RaisedButton(
 child: Text("Login"),
onPressed: () {},
 )
 ],
 ),
 ),

   )
  )
  );
 }
}