import 'package:flutter/material.dart';

void main() => runApp(ButtonColumn());

//membuat class BelajarImage yang secara default extends StatelessWidget
class ButtonColumn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, //menghilangkan banner debug

      home: Scaffold(
        appBar: AppBar(
          title: Text("Flat Button"), //Text di AppBar
          backgroundColor: Colors.blue, //backgroundColor di appBar
          actions: <Widget>[
            Icon(Icons.search),
          ],
        ),
        backgroundColor: Color.fromARGB(
            255, 7, 40, 65), //memberi backgroundColor pada scaffold
        body: Column(
          children: <Widget>[
            RaisedButton(
              color: Colors.amber,
              child: Text("Raised Button"),
              onPressed: () {},
            ),
            MaterialButton(
              color: Colors.lime,
              child: Text("Material Button"),
              onPressed: () {},
            ),
            FlatButton(
              color: Colors.lightGreenAccent,
              child: Text("FlatButton Button"),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
