import 'package:flutter/material.dart';
void main() => runApp(BelajarImage());
//membuat class BelajarImage yang secara default extends StatelessWidget
class BelajarImage extends StatelessWidget {
 @override
 Widget build(BuildContext context) {
 return MaterialApp(
    debugShowCheckedModeBanner: false,//menghilangkan banner debug
   
 home: 
 Scaffold(
  appBar: AppBar(
    title: Text("Image Widget"), //Text di AppBar
    backgroundColor: Color.fromARGB(255, 9, 49, 82),//backgroundColor di appBar
    actions: <Widget> [
    Icon(Icons.search),
  ],
 ),
  backgroundColor: Colors.blue,//memberi backgroundColor pada scaffold
  body: Column(children: [
           SizedBox(height: 60),
           Center(child: Text('Belajar Image Widget', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),)),
           SizedBox(height: 5),
           Center(child: Text('Noga Muktiwati (E41200415) A', style: TextStyle(fontSize: 12, color: Colors.white),)),
           SizedBox(height: 10),
           Image.asset('assets/images/noga.jpg'),
  ],)
   )
  );
 }
}