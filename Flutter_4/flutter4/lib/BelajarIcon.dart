import 'package:flutter/material.dart';
void main() => runApp(BelajarIcon());
//membuat class BelajarImage yang secara default extends StatelessWidget
class BelajarIcon extends StatelessWidget {
 @override
 Widget build(BuildContext context) {
 return MaterialApp(
    debugShowCheckedModeBanner: false,//menghilangkan banner debug
   
 home: 
 Scaffold(
  appBar: AppBar(
    title: Text("Icon Widget"), //Text di AppBar
    backgroundColor: Color.fromARGB(255, 9, 49, 82),//backgroundColor di appBar
    actions: <Widget> [
    Icon(Icons.search),
  ],
 ),
  backgroundColor: Colors.blue,//memberi backgroundColor pada scaffold
  body: Container(
    padding: EdgeInsets.all(16.0),
    child: Column(
      children:[
        SizedBox(height: 70),//memberikan space ruangkosong 
        Center(child: Text('Belajar Icon Widget', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),)),
        SizedBox(height: 10),//memberikan space ruangkosong
        Text('By Noga Muktiwati (E41200415) A', style: TextStyle(fontSize: 15, color: Colors.white),),
        SizedBox(height: 50),
        Image.asset('assets/images/noga.jpg', width: 200, height: 200,),
        SizedBox(height: 100),//memberikan space ruangkosong
        Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
            Column(
              children: <Widget>[
              Icon(Icons.access_alarm, color: Colors.white),
              Text('Alarm', style: TextStyle(fontSize: 15, color: Colors.white),),
              ],
              ),
            Column(
              children: <Widget>[
              Icon(Icons.phone, color: Colors.white),
              Text('Phone', style: TextStyle(fontSize: 15, color: Colors.white),),
                ],
              ),
            Column(
              children: <Widget>[
              Icon(Icons.book, color: Colors.white),
              Text('Book', style: TextStyle(fontSize: 15, color: Colors.white),),
                ],
              ),
      ],
    ),
  ]
      
    )
       
 )

   )
  );
 }
}