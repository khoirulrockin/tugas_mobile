import 'package:flutter/material.dart';
void main() => runApp(BelajarImage());
//membuat class BelajarImage yang secara default extends StatelessWidget
class BelajarImage extends StatelessWidget {
 @override
 Widget build(BuildContext context) {
 return MaterialApp(
    debugShowCheckedModeBanner: false,//menghilangkan banner debug
   
 home: 
 Scaffold(
  appBar: AppBar(
    title: Text("Image Widget"), //Text di AppBar
    backgroundColor: Colors.grey,//backgroundColor di appBar
    actions: <Widget> [
    Icon(Icons.search),
  ],
 ),
  backgroundColor: Colors.grey,//memberi backgroundColor pada scaffold
  body: Column(children: [
           SizedBox(height: 60),
           Center(child: Text('Image Widget', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),)),
           SizedBox(height: 5),
           Center(child: Text('Muhammad Khoirul Rosikin', style: TextStyle(fontSize: 12, color: Colors.yellowAccent),)),
           SizedBox(height: 10),
           Image.asset('assets/images/khoirul.jpeg'),
  ],)
   )
  );
 }
}