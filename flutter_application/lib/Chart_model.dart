class ChartModel {
  final String? name;
  final String? message;
  final String? time;
  final String? profileUrl;

  ChartModel({this.name, this.message, this.time, this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'P Fahmi TIF A',
      message: 'Fah, gimana hasil diksusinya tadi?',
      time: '12.03 A.M',
      profileUrl:
          'https://mir-s3-cdn-cf.behance.net/user/276/b7f80036973007.5eaa8828be329.jpg'),
  ChartModel(
      name: 'P Ila TIF A',
      message: 'Il, desain e udah selesai kah?',
      time: '21/03/2022',
      profileUrl:
          'https://icon-library.com/images/no-profile-picture-icon/no-profile-picture-icon-15.jpg'),
  ChartModel(
      name: 'P Cindy TIF A',
      message: 'Cin tadi ke Basecamp Cafe sama siapa itu cin? cie cie',
      time: '09.45 A.M',
      profileUrl:
          'https://cdn-brilio-net.akamaized.net/news/2017/06/10/127153/637749-selebgram-cantik-go-inter.jpg'),
  ChartModel(
      name: 'P Diana TIF A',
      message: 'Iya gpp din, santai wae',
      time: '13.49 A.M',
      profileUrl:
          'https://th.bing.com/th/id/OIP.9uFGYeBLPU_hAqFo8mFoWAD5D5?pid=ImgDet&rs=1'),
  ChartModel(
      name: 'P Ejak TIF A',
      message: 'Sami-sami',
      time: '02.49 A.M',
      profileUrl:
          'https://icon-library.com/images/no-profile-picture-icon/no-profile-picture-icon-15.jpg'),
];
