import 'package:flutter/material.dart';

void main() => runApp(ButtonContainer());

//membuat class BelajarImage yang secara default extends StatelessWidget
class ButtonContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false, //menghilangkan banner debug

        home: Scaffold(
            appBar: AppBar(
              title: Text("Widget with Container"), //Text di AppBar
              backgroundColor:
                  Colors.grey, //backgroundColor di appBar
              actions: <Widget>[
                Icon(Icons.search),
              ],
            ),
            backgroundColor:
                Colors.grey, //memberi backgroundColor pada scaffold
            body: Column(
              children: [
                SizedBox(height: 60),
                Center(
                    child: Text(
                  'Button Widget',
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                )),
                SizedBox(height: 5),
                Center(
                    child: Text(
                  'Muhammad Khoirul Rosikn',
                  style: TextStyle(fontSize: 12, color: Colors.yellowAccent),
                )),
                SizedBox(height: 10),
                Image.asset('assets/images/khoirul.jpeg'),
                SizedBox(height: 20),
                Container(
                    width: 180,
                    height: 55,
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: BorderRadius.circular(35),
                    ),
                    child: Row(
                      children: [
                        SizedBox(width: 20),
                        Text(
                          'Nazwa Maharivi',
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.yellow,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ))
              ],
            )));
  }
}
