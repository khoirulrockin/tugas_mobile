import 'package:flutter/material.dart';

void main() => runApp(BelajarBotton());

class BelajarBotton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Icon Widget"),
          backgroundColor: Colors.blue,
        ),
        body: Column(
            children: <Widget>[
              RaisedButton(
                color: Colors.amber,
                child: Text("Raised Button"),
                onPressed: () {},
              ),
              MaterialButton(
                color: Colors.lime,
                child: Text("Material Button"),
                onPressed: () {},
              ),
              FlatButton(
                color: Colors.lightGreenAccent,
                child: Text("FlatButton Button"),
                onPressed: () {},
              ),
            ],
          )
      ),
    );
  }
}
