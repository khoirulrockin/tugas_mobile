import 'package:flutter/material.dart';
void main() => runApp(FormInput());
//membuat class BelajarImage yang secara default extends StatelessWidget
class FormInput extends StatelessWidget {
 @override
 Widget build(BuildContext context) {
 return MaterialApp(
    debugShowCheckedModeBanner: false,//menghilangkan banner debug
   
 home: 
 Scaffold(
  appBar: AppBar(
    title: Text("Form Login"), //Text di AppBar
    backgroundColor: Colors.grey,//backgroundColor di appBar
    actions: <Widget> [
    Icon(Icons.search),
  ],
 ),
 
  body: Padding(
 padding: const EdgeInsets.all(8.0),
 child: Form(
 child: Column(
 children: <Widget>[
 TextFormField(
 decoration: InputDecoration(hintText: "Username"),
 ),
 TextFormField(
 obscureText: true,
decoration: InputDecoration(hintText: "Password"),
 ),
 RaisedButton(
 child: Text("Login"),
onPressed: () {},
 )
 ],
 ),
 ),

   )
  )
  );
 }
}